import React from 'react';
import { Link, Redirect } from "react-router-dom";
import { connect } from 'react-redux'
import { login } from '../actions/loginAction'
import "../assets/css/app.css"

function Login() {
    const [isLoggedIn, setLoggedIn] = React.useState(false);
    const [state, setState] = React.useState({
        email: "",
        password: ""
    })

    const handleOnchange = e => {
        const { name, value } = e.target
        setState(prevState => ({
            ...prevState,
            [name]: value
        }))
    }
    const onSubmit = (e) => {
        e.preventDefault()
        setLoggedIn(true)
    }
    if (isLoggedIn) {
        return <Redirect to="/dashboard" />;
    }
    return (
        <main className="d-flex w-100">
            <div className="container d-flex flex-column">
                <div className="row vh-100">
                    <div className="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
                        <div className="d-table-cell align-middle">

                            <div className="text-center mt-4">
                                <p className="lead">
                                    Sign in to your account to continue
							    </p>
                            </div>

                            <div className="card">
                                <div className="card-body">
                                    <div className="m-sm-4">
                                        <form>
                                            <div className="form-group">
                                                <label>Email</label>
                                                <input className="form-control form-control-lg" type="email" name="email" placeholder="Enter your email" onChange={handleOnchange} />
                                            </div>
                                            <div className="form-group">
                                                <label>Password</label>
                                                <input className="form-control form-control-lg" type="password" name="password" placeholder="Enter your password" onChange={handleOnchange} />
                                                <small>
                                                    <Link to="/reset-password">Forgot password?</Link>
                                                </small>
                                            </div>
                                            <div className="text-center mt-3">
                                                <button type="submit" className="btn btn-lg btn-primary" onClick={onSubmit} >Sign in</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </main>
    )
}

const mapStateToProp = state => ({
    loggedIn: state.loggedIn.loggedIn
})
export default connect(
    mapStateToProp,
    {login}
)(Login)