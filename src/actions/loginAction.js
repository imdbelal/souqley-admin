import { LOG_IN } from './types'

export const login = () => dispatch => {
    dispatch({
        type: LOG_IN,
        payload: true
    })
}