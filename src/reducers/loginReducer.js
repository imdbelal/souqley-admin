import {LOG_IN} from '../actions/types'

const initailState = {
    loggedIn: false
}

export default function(state = initailState, action){
    if(action.type == LOG_IN){
        return {
            ...state,
            loggedIn: action.payload
        }
    }else{
        return state
    }
}