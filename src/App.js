import React from 'react';
import { BrowserRouter as Router, Link, Route } from "react-router-dom"
import { Provider } from 'react-redux'
import store from './store.js'

import { AuthContext } from "./context/auth";
import Login from './components/Login'
import Dashboard from './components/Dashboard'

import './App.css';

function App(props) {
  return (
    <Provider store={store}>
      <AuthContext.Provider value={false}>
        <Router>
          <div>
            {/* <Route exact path="/" component={Home} />
          <PrivateRoute path="/admin" component={Admin} /> */}

            <Route path="/dashboard" component={Dashboard} />
            <Route path="/" component={Login} />
          </div>
        </Router>
      </AuthContext.Provider>
    </Provider>
  );
}

export default App;
